﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Tool_path_creator", menuName = "TowerDefense/Tool_path_creator")]
public class Tool_path_creator : ScriptableObject {

    public GameObject tr_2;
    public GameObject tr_angle;
    public GameObject tr_3;
    public GameObject tr_4;
    [ContextMenu("Create_path")]
    public void Create_path()
    {
        var Tiles_data = new List<Tile_data>();
        var root = GameObject.FindGameObjectWithTag(Tags.Pathes_root).GetComponent<Transform>();
        var world = GameObject.FindGameObjectWithTag(Tags.Controllers).GetComponent<World_info>();
        var pathes_start = new int[root.childCount];
        for (int i = 0; i < root.childCount; i++)
        {
            pathes_start[i] = -1;
            var path = root.GetChild(i);
            if(!path.gameObject.activeSelf) continue;
            pathes_start[i] = Tiles_data.Count;
            for (int y = 0; y < path.childCount - 1; y++)
            {
                var count = Mathf.Round( Vector3.Distance( path.GetChild(y).position, path.GetChild( y + 1 ).position ));
                var dir = Vector3.Normalize(path.GetChild(y + 1).position - path.GetChild( y ).position);
                var start = Vector3Int.FloorToInt(path.GetChild(y).position);
                for (int tile_i = 0; tile_i < count; tile_i++)
                {
                    Tiles_data.Add( new Tile_data( Tiles_data.Count + 1, false, start + dir * tile_i , i) );
                }          
            }
            Tiles_data.Add( new Tile_data( Tiles_data.Count + 1, true, path.GetChild( path.childCount - 1 ).position , i ) );
        }
        world.Tiles_data = Tiles_data.ToArray();
        world.Pathes_start = pathes_start;

        HashSet<Vector3Int> tiles_mesh = new HashSet<Vector3Int>();
        var t_parent = GameObject.FindWithTag(Tags.Root_tiles).transform;
        var t_ch_len = t_parent.childCount;
        for (int i = 0; i < t_ch_len; i++)
        {
            DestroyImmediate(t_parent.GetChild(0).gameObject);
        }
        
        for (int i = 0; i < Tiles_data.Count - 1; i++)
        {
            if(!tiles_mesh.Contains(Tiles_data[i].Cur_position))
                tiles_mesh.Add(Tiles_data[i].Cur_position);
        }

        GameObject tr_mesh = root.gameObject;
        var directions = new bool[4]; // +z , +x, -z, -x
        int ii = 0;

        var q_0 = Quaternion.Euler(-90,0,0);
        var q_90 = Quaternion.Euler(-90,90,0);
        var q_180 = Quaternion.Euler(-90,180,0);
        var q_270 = Quaternion.Euler(-90,270,0);

        var v_zp = new Vector3Int(0,0,1);
        var v_zn = new Vector3Int(0,0,-1);
        var v_xp = new Vector3Int(1,0,0);
        var v_xn = new Vector3Int(-1,0,0);

        foreach (var item in tiles_mesh)
        {

            directions[0] = tiles_mesh.Contains(item + v_zp);
            directions[1] = tiles_mesh.Contains(item + v_xp);
            directions[2] = tiles_mesh.Contains(item + v_zn);
            directions[3] = tiles_mesh.Contains(item + v_xn);

            if(directions[0] && directions[1] && directions[2] && directions[3])
                tr_mesh = Instantiate(tr_4,item,q_0,t_parent);
            
            else if( directions[0] && directions[1] && directions[2] || directions[3] && directions[1] && directions[2] || directions[0] && directions[3] && directions[2] || directions[0] && directions[1] && directions[3])
            {
                var r = q_0;
                if(!directions[3])  r = q_90;
                if(!directions[0])  r = q_180;
                if(!directions[1])  r = q_270;

                tr_mesh = Instantiate(tr_3,item,r,t_parent);
            }
            else
            {
                bool angle = false;
                if(directions[2] && directions[3]) { tr_mesh = Instantiate(tr_angle,item,q_0,t_parent);   angle = true; }
                if(directions[3] && directions[0]) { tr_mesh = Instantiate(tr_angle,item,q_90,t_parent);  angle = true; }
                if(directions[0] && directions[1]) { tr_mesh = Instantiate(tr_angle,item,q_180,t_parent); angle = true; }
                if(directions[1] && directions[2]) { tr_mesh = Instantiate(tr_angle,item,q_270,t_parent); angle = true; }

                if((directions[2] || directions[0]) && !angle) tr_mesh = Instantiate(tr_2,item,q_0,t_parent);
                if((directions[1] || directions[3]) && !angle) tr_mesh = Instantiate(tr_2,item,q_90,t_parent);



            }
            tr_mesh.name = "Id " + ii;
            ii++;
        } 

        Debug.Log("Done");
    }
}

