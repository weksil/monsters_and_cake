﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cristal_Animator : MonoBehaviour
{
    public Transform Cristall;
    public bool Constant_Rotate;
    public Vector3 Rotate_speed;
    public float Vawe_speed;
    public float Vawe_delta;
    private float start_height;
    private float vawe_direction;
    private Vector3 vawe_vector ;
    private float rotate_time;
    private Vector3 rotate_vector;

    private void Start() {
        vawe_direction = 1f;
        start_height = Cristall.localPosition.y;
        vawe_vector = new Vector3(0, Vawe_speed * vawe_direction ,0);
        rotate_vector = GenerateVector3(40,60,Rotate_speed);
        rotate_time = Random.Range(1f,2f);
    }

    // Update is called once per frame
    void Update()
    {
        if( ( Cristall.localPosition.y <= start_height && vawe_direction < 0 ) || ( Cristall.localPosition.y >= ( start_height + Vawe_delta ) && vawe_direction > 0) )
        {
            vawe_direction *= -1;
            vawe_vector.y = Vawe_speed * vawe_direction;
        }
        if(!Constant_Rotate)
        {
            if(rotate_time < 0)
            {      
                rotate_vector = GenerateVector3(40,120,Rotate_speed);
                rotate_time = Random.Range(1f,2f);
            }
            rotate_time -= Time.deltaTime;
        }
        Cristall.Translate(vawe_vector * Time.deltaTime,Space.World);
        Cristall.Rotate(rotate_vector * Time.deltaTime,Space.World);
    }
    private Vector3 GenerateVector3( float min, float max, Vector3 speed) => new Vector3(
                                                                                            Random.Range(min,max) * speed.x,
                                                                                            Random.Range(min,max) * speed.y,
                                                                                            Random.Range(min,max) * speed.z
                                                                                        );


}