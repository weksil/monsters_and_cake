using UnityEngine;
public interface ITowerLogic {
    void CustomUpdate(World_info world,float deltaTime,float timeSpeed);
    Mob_data_mono GetTartget();
    Vector3 GetRange();
}