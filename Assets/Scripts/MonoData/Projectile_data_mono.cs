﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_data_mono : MonoBehaviour
{
    public Mob_data_mono Target;
    public Vector3 EndPoint;
    public int Damage;
    public float Radius;
}
