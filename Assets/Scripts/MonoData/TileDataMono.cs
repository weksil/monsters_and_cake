﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileDataMono : MonoBehaviour
{
    public int Next_id;
    public bool Is_finish;
    public Vector3 Cur_position;

    #if UNITY_EDITOR
    public void Init(int next, bool finish)
    {
        Next_id = next;
        Cur_position = transform.position;
        Is_finish = finish;
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + new Vector3(0,0.5f,0), new Vector3(1, 1, 1));

    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position + new Vector3(0,0.5f,0), new Vector3(1, 1, 1));
    }
    #endif
}
