﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Mob_data_mono : MonoBehaviour
{
    [HideInInspector]public int Current_tile;
    public int Hp = 100;
    private float hp_base;
    public int Damage;
    public int Price;
    [HideInInspector]public Vector3 Direction;
    [Space]
    public Transform literal_parent;
    public Transform Pivot;
    public Image Hp_bar;
    [HideInInspector] public bool addMoney = false;
    public void Move(float delta) => transform.Translate(Direction * Vawe.mob_speed * delta,Space.World);
    private void Start() {
        hp_base = Hp;
    }
    public void Getdamage(int damage)
    {
        Numeric_drawer.instance.Print(damage,literal_parent,0.5f);
        Hp -= damage;
        Hp_bar.fillAmount = Hp/hp_base;
        if(Hp <= 0)
        {
            addMoney = true;
        }
    }
}
