﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_logic_mono : MonoBehaviour, ITowerLogic
{
    public float Range;
    public float AoeRadius;
    public float ReloadTime;
    public int Damage;
    public GameObject Projectile;
    public Transform Projectile_emmitor;
    private float wait;
    private Mob_data_mono target;
    public void CustomUpdate(World_info world, float deltaTime, float timeSpeed)
    {
        if(wait <= ReloadTime)
        {
            wait += deltaTime * timeSpeed;
            return;
        }

        var range = Range * Range;
        float min_l = float.MaxValue;
        int min_index = -1;
        for (int i = 0; i < world.Mobs_data.Count; i++)
        {
            if( Tools.PowerDistance(world.Mobs_data[i].transform.position, transform.position) <= range)
            {
                float r = Tools.PowerDistance(world.Mobs_data[i].transform.position, world.Cake_tr.position);
                if(r < min_l)
                {
                    min_index = i;
                    min_l = r;
                }
            }
        }
        if(min_index > -1)
        {
            Attack(world.Mobs_data[min_index]);
            wait = 0;
            target = world.Mobs_data[min_index];
        }
    }

    public Vector3 GetRange() => new Vector3(Range*2,Range*2,1);

    public Mob_data_mono GetTartget() => target;

    private void Attack(Mob_data_mono mob_data_mono)
    {
        Projectile_controller.instance.InstantiateProjectile(this,mob_data_mono);
        
    }

    #if UNITY_EDITOR

    void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(transform.position,Range);
    }
    void OnDrawGizmosSelected()
    {
        var t_pos = target == null ? transform.position + new Vector3(0,0,AoeRadius) : target.Pivot.position;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,Range);
        Gizmos.DrawWireSphere(t_pos,AoeRadius);
    }

    #endif
}
