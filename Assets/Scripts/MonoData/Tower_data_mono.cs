﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_data_mono : MonoBehaviour
{
    public int Id;
    public int Level;
    public bool Is_max_lvl;
    public int Price = 100;
    public int Ugrade_Price = 100;

    [Space]
    public ITowerLogic Logic;
    public Tower_animator_mono Animator;

    public void Init()
    {
        Logic = GetComponent<ITowerLogic>();
    }

    public void CustomUpdate(World_info world,float deltaTime, float speed)
    {
        Logic.CustomUpdate(world,deltaTime,speed);
        Animator?.CustomUpdate(deltaTime,Logic.GetTartget());
    }

}