﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_animator_mono : MonoBehaviour
{
    public Transform Bone;
    public float ShiftAngle;
    public void CustomUpdate(float deltaTime, Mob_data_mono target)
    {
        if(target == null) return;
        var target_point = target.Pivot.position - Bone.position;
        var angle = Mathf.Atan2(-target_point.z,target_point.x) * Mathf.Rad2Deg + ShiftAngle;
        Bone.rotation = Quaternion.Euler(Bone.rotation.eulerAngles.x,angle,Bone.rotation.eulerAngles.z);

    }
}
