using System;
using UnityEngine;

[Serializable]
public struct Mob_count_pair
{
    public int Path;
    public int Count;
    public GameObject Mob;
}