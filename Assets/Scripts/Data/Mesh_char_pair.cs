using UnityEngine;
using System;

[Serializable]
public struct Mesh_char_pair {
    public char Key;
    public Mesh Value;
}