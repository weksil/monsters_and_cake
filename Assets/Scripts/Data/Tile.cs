using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Tile_data
{
    public int Path_id;
    public int Next_id;
    public bool Is_finish;
    public Vector3Int Cur_position;
    public Tile_data(int next, bool finish, Vector3 position, int path_id)
    {
        Next_id = next;
        Cur_position = Vector3Int.FloorToInt(position);
        Is_finish = finish;
        Path_id = path_id;
    }
}