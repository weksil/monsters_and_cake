using System;
using UnityEngine;

[Serializable]
public class Vawe
{
    public const float mob_speed = 1.5f;
    public const float time_between_mob = 1/mob_speed + 0.5f;
    public float Wait_time;
    public Mob_count_pair[] Mobs;
}