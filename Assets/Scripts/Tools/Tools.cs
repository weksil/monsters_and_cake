using UnityEngine;

public class Tools {
    public static float PowerDistance(Vector3 a, Vector3 b) => ( b.x - a.x ) * ( b.x - a.x ) +  (b.z - a.z ) * ( b.z - a.z );
}