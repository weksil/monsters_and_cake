﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path_gizmo : MonoBehaviour
{
    public World_info world;
    public Color Path_color;
    void OnDrawGizmosSelected()
    {
        Draw(Color.red);
    }
    void OnDrawGizmos()
    {
        Draw(Path_color);
    }
    void Draw(Color color)
    {
        Gizmos.color = color;
        int p_id = transform.GetSiblingIndex();
        var c_tr = transform;

        if(world == null || world.Tiles_data == null || world.Tiles_data.Length == 0 || world.Pathes_start.Length <= p_id || world.Pathes_start[p_id] == -1)
        {
            for (int i = 0; i < c_tr.childCount - 1; i++)
            {
                Gizmos.DrawWireCube(c_tr.GetChild(i).position + new Vector3(0,0.5f,0), Vector3.one ); 
                Gizmos.DrawLine(c_tr.GetChild(i).position + new Vector3(0,0.5f,0), c_tr.GetChild(i + 1).position + new Vector3(0,0.5f,0) ); 
            }
            Gizmos.DrawWireCube(c_tr.GetChild(c_tr.childCount - 1).position + new Vector3(0,0.5f,0), Vector3.one ); 
        }
        else
        {
            for (int i = world.Pathes_start[p_id]; i < world.Tiles_data.Length; i++)
            {
                if(world.Tiles_data[i].Path_id != p_id)
                    break;
                Gizmos.DrawWireCube( world.Tiles_data[i].Cur_position + new Vector3(0,0.5f,0), Vector3.one ); 
            }
        }

        
    }
}
