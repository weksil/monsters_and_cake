﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading_screen : MonoBehaviour
{
    public Level_db db;
    private void Start() {
        StartCoroutine(load_wait());
    }
    IEnumerator load_wait()
    {
        yield return new WaitForSeconds(1.5f);
        db.Load_lvl();
    }
}
