﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Creator : MonoBehaviour
{
    public Tower_db t_db;
    public Tower_controller tower_Controller;
    public Tower_data_mono CreateTower(int id, Transform cur_transform) => ChangeTower(id,0,cur_transform);
    public Tower_data_mono UpgradeTower(int id, int level, Transform cur_transform) =>  ChangeTower(id, level,cur_transform);
    public Tower_data_mono DestroyTower(Transform cur_transform, Tower_data_mono data)
    {
        tower_Controller.DestroyTower(data);
        return ChangeTower(0, 0, cur_transform);
    }
    private Tower_data_mono ChangeTower(int new_id, int lvl, Transform cur_transform)
    {
        var nw_tower = Instantiate(t_db.Levels[new_id].levels[lvl],cur_transform,false).GetComponent<Tower_data_mono>();
        nw_tower.transform.localPosition = Vector3.zero;
        nw_tower.transform.rotation = Quaternion.Euler(0,0,0);
        tower_Controller.AddTower(nw_tower);
        return nw_tower;
    }

}
