﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World_info : MonoBehaviour
{

    public int Helth_cake_base;
    private int Helth_cake;
    public Transform Cake_tr;
    public Transform Cake_literal_tr;
    public Tile_data[] Tiles_data;
    [HideInInspector] public int[] Pathes_start;
    public List<Mob_data_mono> Mobs_data;
    private List<int> clear_indexes = new List<int>();
    public event Action<int,int,int> GetDamage_e; // damage, current hp, base hp
    public event Action<int> Change_coins_e;
    public event Action Die_mob_e;
    [Space]
    public int coins = 100;
    private void Start() {
        Helth_cake = Helth_cake_base;
    }
    public float GetCakeHealth() => Helth_cake/(float)Helth_cake_base;
    public bool CakeAlive() => Helth_cake > 0;
    private void LateUpdate() {
        clear_indexes.Clear();
        for (int i = 0; i < Mobs_data.Count; i++)
        {
            if(!Mobs_data[i].gameObject.activeSelf)
                clear_indexes.Add(i);
        }
        for (int i = clear_indexes.Count - 1; i >= 0 ; i--)
        {
            Mobs_data.RemoveAt(clear_indexes[i]);
            Die_mob_e();
        }
    }
    public void Add_mob(Mob_data_mono data, int tile)
    {
        Mobs_data.Add(data);
        data.Current_tile = tile;
    } 
    public void Get_damage(int damage)
    {
        Helth_cake -= damage;
        GetDamage_e(damage, Helth_cake, Helth_cake_base);
        // Numeric_drawer.instance.Print(damage,Cake_literal_tr,0.5f);
    }
    public void DestroyMob(Mob_data_mono mob)
    {
        mob.gameObject.SetActive(false);
        if(mob.addMoney)
            AddCoins(mob.Price);
    }
    public void AddCoins(int coin)
    {
        coins += coin;
        Change_coins_e(coins);
    }
    public int GetCoins() => coins;
    #if UNITY_EDITOR
    #endif
}
