﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_controller : MonoBehaviour
{
    public World_info World;
    public List<Tower_data_mono> Towers;
    public Time_controller time_Controller;
    private List<int> clear_indexes = new List<int>();
    private void LateUpdate() {
        clear_indexes.Clear();
        for (int i = 0; i < Towers.Count; i++)
        {
            if(!Towers[i].gameObject.activeSelf)
                clear_indexes.Add(i);
        }
        for (int i = clear_indexes.Count - 1; i >= 0 ; i--)
        {
            Towers.RemoveAt(clear_indexes[i]);
        }
    }

    private void Start() 
    {
        for (int i = 0; i < Towers.Count; i++)
        {
            Towers[i].Init();
        }
    }
    private void Update() 
    {
        if( time_Controller.Is_pause ) return;
        float d = Time.unscaledDeltaTime;
        for (int i = 0; i < Towers.Count; i++)
        {
            Towers[i].CustomUpdate(World,d,time_Controller.GetSpeed());
        }
    }

    public void AddTower(Tower_data_mono tower)
    {
        tower.Init();
        Towers.Add(tower);
        World.AddCoins(-tower.Price);
    }
    public void DestroyTower(Tower_data_mono tower)
    {
        World.AddCoins(tower.Price/2);
    }
}
