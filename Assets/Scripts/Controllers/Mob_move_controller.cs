﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob_move_controller : MonoBehaviour
{
    public World_info World;
    private const float half_tile_size_power = 1.1f;
    private void FixedUpdate() {


        float delta_time = Time.deltaTime;
        for (int i = 0; i < World.Mobs_data.Count; i++)
        {
            var mob = World.Mobs_data[i];
            var cur_tile = World.Tiles_data[mob.Current_tile];
            if(!cur_tile.Is_finish)
            {
                mob.Move(delta_time);
                var next_tile = World.Tiles_data[ cur_tile.Next_id ];
                var path_lenth_v3 = next_tile.Cur_position - mob.transform.position;
                var path_lenth = path_lenth_v3.x * path_lenth_v3.x +
                                 path_lenth_v3.z * path_lenth_v3.z ;
                                 
                if(path_lenth < half_tile_size_power)
                {
                    mob.Current_tile = cur_tile.Next_id;
                    mob.Direction = Vector3.Normalize(next_tile.Cur_position - mob.transform.position);
                    mob.transform.LookAt(next_tile.Cur_position,Vector3.up);
                }
            }
            else
            {
                if(!mob.gameObject.activeSelf) return;
                World.Get_damage(mob.Damage);
                World.DestroyMob(mob);
            }
        }
    }
}
