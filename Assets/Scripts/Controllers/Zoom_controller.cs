﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom_controller : MonoBehaviour
{
    public Camera TargetCamera;
    public float fly_speed;
    public float zoom_speed = 2;
    public float min_zoom = 2;
    private float BaseSize;
    private Vector3 shift;
    private Vector3 zero;
    private Transform target;
    public float tests_dz;
    private void Start() 
    {
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(TargetCamera.transform.position,TargetCamera.transform.forward,out hit,100,16); // 16 =  2 ^ 4(Water_layer)
        zero = hit.point;
        BaseSize = TargetCamera.orthographicSize;
        shift = TargetCamera.transform.position - zero  ;
    }


    private void Update() 
    {
        Vector3 dt;
        float dz;
        if(target != null)
        {
            dt = fly_speed * Time.unscaledDeltaTime * ( target.position - TargetCamera.transform.position + shift );
            dz = Time.unscaledDeltaTime * zoom_speed * (TargetCamera.orthographicSize - min_zoom) ;
        }
        else
        {
            
            dt = fly_speed * Time.unscaledDeltaTime * ( zero - TargetCamera.transform.position + shift ) ;
            dz = Time.unscaledDeltaTime * zoom_speed * ( TargetCamera.orthographicSize - BaseSize) ;
        }
        // dz = (float)Math.Round(dz,3);
        // tests_dz = dz;
        // if(dz < 0.001f && dz > -0.001f) dz = 0;
        dt.y = 0;
        TargetCamera.orthographicSize -= dz;
        TargetCamera.transform.Translate( dt, Space.World);

    }
    public void SetTarget(Transform target) => this.target = target?.parent;
}
