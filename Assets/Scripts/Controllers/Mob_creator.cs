﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob_creator : MonoBehaviour
{
    public Level_db level_db;
    public Main_game_ui ui_controller;
    public World_info World;
    private int mob_count;
    public Vawe[] Vawes;
    public GameObject[] Dangerous_marks;
    private void Start() {
        World.Die_mob_e += Die_Mob;
        mob_count = 0;
        for (int i = 0; i < Vawes.Length; i++)
        {
            for (int y = 0; y < Vawes[i].Mobs.Length; y++)
            {
                mob_count += Vawes[i].Mobs[y].Count;
            }
        }
        for (int i = 0; i < Dangerous_marks.Length; i++)
        {
            Dangerous_marks[i].SetActive(false);
        }
        StartCoroutine(Logic());
    }
    private void Die_Mob()
    {
        mob_count --;
        if(mob_count == 0 || World.GetCakeHealth() <= 0)
        {
            ui_controller.GameOver(World.GetCakeHealth(),World.CakeAlive());
        }
    }


    public IEnumerator Logic()
    {
        for (int i = 0; i < Vawes.Length; i++)
        {

            for (int y = 0; y < Vawes[i].Mobs.Length; y++)
            {
                Dangerous_marks[Vawes[i].Mobs[y].Path].SetActive(true);
            }
            
            yield return WaitTime( Vawes[i].Wait_time );

            for (int y = 0; y < Vawes[i].Mobs.Length; y++)
            {
                var mob_pair = Vawes[i].Mobs[y];
                var start_i = World.Pathes_start[ mob_pair.Path ];
                var pos = World.Tiles_data[ start_i ].Cur_position;
                Dangerous_marks[mob_pair.Path].SetActive(false);
                for (int mob_i = 0; mob_i < mob_pair.Count; mob_i++)
                {
                    var mob = Instantiate(mob_pair.Mob, pos, Quaternion.identity).GetComponent<Mob_data_mono>();
                    World.Add_mob(mob,start_i);
                    yield return WaitTime(Vawe.time_between_mob);
                }
                
            }
        }
    }

    private IEnumerator WaitTime(float time)
    {
        bool state = true;
        while (state)
        {
            if ( enabled )
                time -= Time.deltaTime;
            state = time > 0;
            yield return null;
        }
    }
}
