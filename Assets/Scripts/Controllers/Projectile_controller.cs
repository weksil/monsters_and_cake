﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_controller : MonoBehaviour
{
    public World_info World;
    public List<Projectile_data_mono> Projectiles;
    public float PrjSpeed;
    private const float death_dist = 0.8f;
    private const float prj_speed = 12f;
    private List<int> clear_indexes = new List<int>();
    public static Projectile_controller instance;
    private void Start() {
        instance = this;
    }

    private void FixedUpdate() {
        var dt = Time.deltaTime;
        var speed = dt * prj_speed * Vector3.forward;
        for (int i = 0; i < Projectiles.Count; i++)
        {
            var prj = Projectiles[i];
            if(!prj.gameObject.activeSelf) continue;
            if(Tools.PowerDistance(prj.transform.position,prj.EndPoint) <= death_dist)
            {
                var dist = prj.Radius * prj.Radius;
                for (int y = 0; y < World.Mobs_data.Count; y++)
                {
                    if (Tools.PowerDistance(World.Mobs_data[y].transform.position, prj.EndPoint) < dist)
                    {
                        World.Mobs_data[y].Getdamage(prj.Damage);
                        if(World.Mobs_data[y].Hp <= 0 && World.Mobs_data[y].gameObject.activeSelf)
                            World.DestroyMob(World.Mobs_data[y]);
                    }
                }
                prj.gameObject.SetActive(false);
                continue;
            }

            if(prj.Target != null)
                prj.EndPoint = prj.Target.Pivot.position;
            prj.transform.LookAt(prj.EndPoint,Vector3.up);
            prj.transform.Translate(speed,Space.Self);
        }
    }
    private void LateUpdate() {
        clear_indexes.Clear();
        for (int i = 0; i < Projectiles.Count; i++)
        {
            if(!Projectiles[i].gameObject.activeSelf)
                clear_indexes.Add(i);
        }
        for (int i = clear_indexes.Count - 1; i >= 0 ; i--)
        {
            Destroy(Projectiles[clear_indexes[i]].gameObject);
            Projectiles.RemoveAt(clear_indexes[i]);
        }
    }
    public void InstantiateProjectile(Tower_logic_mono tower, Mob_data_mono target)
    {
        var prj = Instantiate(tower.Projectile, tower.Projectile_emmitor.position, Quaternion.identity);
        prj.transform.LookAt(target.transform.position,Vector3.up);
        var data = prj.GetComponent<Projectile_data_mono>();
        data.Radius = tower.AoeRadius;
        data.Damage = tower.Damage;
        data.Target = target;
        data.EndPoint = target.Pivot.position;
        Projectiles.Add(data);
    }

}
