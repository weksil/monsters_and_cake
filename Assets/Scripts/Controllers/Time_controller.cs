﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Time_controller : MonoBehaviour
{
    private int[] time_speed = {1,2,3}; 
    private int gear = 0;
    private bool is_pause = false;
    public bool Is_pause = false;
    private void OnEnable() {
        Time.timeScale = 1;
    }
    private void OnDisable() {
        Time.timeScale = 1;
    }
    public bool Pause()
    {
        is_pause = !is_pause;
        Time.timeScale = is_pause ? 0 : time_speed[gear];
        Is_pause = is_pause;
        return is_pause;
    }

    public int NextGear()
    {
        gear = (gear + 1) % time_speed.Length;
        if(!is_pause)
            Time.timeScale = time_speed[gear];
            
        return time_speed[gear];
    }
    public float GetSpeed() => time_speed[gear];
}
