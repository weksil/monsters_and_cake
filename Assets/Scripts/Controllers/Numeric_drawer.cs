﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Numeric_drawer : MonoBehaviour
{
    public Transform tr_Camera;
    public GameObject prefab;
    public GameObject Container;
    public Mesh[] Numeric_meshes;
    private Queue<MeshFilter> Pool_numeric;
    private Queue<Transform> Pool_container;
    private List<Numeric_container_data> Containers;
    private Vector3 shift_numeric = new Vector3(-0.285f,0,0);
    private Vector3 axis = Vector3.up;
    public static Numeric_drawer instance;
    private void Start() {
        instance = this;
        Pool_numeric = new Queue<MeshFilter>(32);
        Pool_container = new Queue<Transform>(32);
        Containers = new List<Numeric_container_data>();
        for (int i = 0; i < 10; i++)
        {
            CreateNumeric();
        }
        for (int i = 0; i < 5; i++)
        {
            CreateContainer();
        }
    }
    public void Print(int number, Transform parent, float lifeTime, bool random = false)
    {
        var data = new Numeric_container_data();
        data.LifeTime = lifeTime;
        data.Down_speed = 0.5f/lifeTime;
        data.transform = GetContainer();
        Containers.Add(data);

        var str = number.ToString();
        var start_pos = -((str.Length >> 1 ) - 0.5f) * shift_numeric;
        for (int i = 0; i < str.Length; i++)
        {
            var literal = GetNumeric();
            literal.mesh = GetMesh(str[i]);
            var tr = literal.transform;
            tr.SetParent(data.transform);
            tr.localScale = Vector3.one;
            tr.localRotation = Quaternion.identity;
            tr.localPosition = start_pos + shift_numeric * i;
            tr.gameObject.SetActive(true);
        }
        var p_angles = parent.eulerAngles;

        data.transform.SetParent(parent);
        data.transform.localScale = Vector3.one*2;
        data.transform.localPosition = random ? new Vector3(Random.Range(-1f,1f),0,Random.Range(-1f,1f)) : Vector3.zero;
        data.transform.rotation = Quaternion.AngleAxis(tr_Camera.eulerAngles.y - 180,axis);
    }
    private void CreateNumeric()
    {
        var item = Instantiate(prefab);
        item.gameObject.SetActive(false);
        Pool_numeric.Enqueue(item.GetComponent<MeshFilter>());

        item = Instantiate(prefab);
        item.gameObject.SetActive(false);
        Pool_numeric.Enqueue(item.GetComponent<MeshFilter>());

        item = Instantiate(prefab);
        item.gameObject.SetActive(false);
        Pool_numeric.Enqueue(item.GetComponent<MeshFilter>());
    }
    private void CreateContainer()
    {
        Pool_container.Enqueue(Instantiate(Container).transform);
        Pool_container.Enqueue(Instantiate(Container).transform);
        Pool_container.Enqueue(Instantiate(Container).transform);
    }
    private Transform GetContainer()
    {
        if(Pool_container.Count == 0) 
            CreateContainer();
        
        return Pool_container.Dequeue();
    }
    private MeshFilter GetNumeric()
    {
        if(Pool_numeric.Count == 0) 
            CreateNumeric();
        
        return Pool_numeric.Dequeue();
    }
    private Mesh GetMesh(char c)
    {
        switch (c)
        {
            case '0': return Numeric_meshes[0];
            case '1': return Numeric_meshes[1];
            case '2': return Numeric_meshes[2];
            case '3': return Numeric_meshes[3];
            case '4': return Numeric_meshes[4];
            case '5': return Numeric_meshes[5];
            case '6': return Numeric_meshes[6];
            case '7': return Numeric_meshes[7];
            case '8': return Numeric_meshes[8];
            case '9': return Numeric_meshes[9];
            default: return null;
        }
    }
    private Vector3 one = Vector3.one;
    private void Update() {
        var dt = Time.unscaledDeltaTime;
        for (int i = 0; i < Containers.Count; i++)
        {
            var item = Containers[i];
            item.transform.LookAt(tr_Camera,axis);
            item.LifeTime -= dt;
            item.transform.localScale -= item.Down_speed * dt * one; 
            item.transform.Translate(axis * dt,Space.World);
            // var p_angle = item.transform.parent.eulerAngles.y;
            
            // var angle = 180;

            // // if(p_angle > 45 && p_angle < 135) angle = 90;
            // // else
            // //     if(p_angle < 225) angle = 180;
            // //     else
            // //         if(p_angle < 315) angle = 90;

            // item.transform.rotation = Quaternion.AngleAxis(130,axis);
            
            Containers[i] = item;
        }
    }
    private List<int> clear_indexes = new List<int>();
    private void LateUpdate() {
        clear_indexes.Clear();
        for (int i = 0; i < Containers.Count; i++)
        {
            if(Containers[i].LifeTime < 0)
                clear_indexes.Add(i);
        }
        for (int i = clear_indexes.Count - 1; i >= 0 ; i--)
        {
            var container_tr = Containers[clear_indexes[i]].transform;
            var literals = new Transform[container_tr.childCount];
            for (int y = 0; y < container_tr.childCount; y++)
            {
                var literal_tr = container_tr.GetChild(y);
                literals[y] = literal_tr;
                literal_tr.gameObject.SetActive(false);
                Pool_numeric.Enqueue(literal_tr.GetComponent<MeshFilter>());
            }

            for (int y = 0; y < container_tr.childCount; y++)
            {
                literals[y].SetParent(null);
            }
            Pool_container.Enqueue(container_tr);
            Containers.RemoveAt(clear_indexes[i]);
            container_tr.SetParent(null);
        }
    }
}
