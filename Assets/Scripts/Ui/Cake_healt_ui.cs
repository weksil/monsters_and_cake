﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cake_healt_ui : MonoBehaviour
{
    public World_info World;
    public Image Bar;
    private void Start() {
        World.GetDamage_e += GetDamage;
        Bar.transform.parent.rotation = Quaternion.Euler(0,Camera.main.transform.eulerAngles.y - 180,0);
    }

    private void GetDamage(int damage, int hp_cur, int hp_base)
    {
        Numeric_drawer.instance.Print(damage,World.Cake_literal_tr,0.8f,true);
        Draw( (float)hp_cur / (float)hp_base );
    }

    public void Draw(float part) => Bar.fillAmount = part;
}
