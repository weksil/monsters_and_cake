﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_global_ui : MonoBehaviour
{
    private Tower_ui selected_tower;
    public Zoom_controller zoom_Controller;
    public Tower_Creator creator;
    public World_info world;

    private void Awake() {
        var Containers = GameObject.FindObjectsOfType<Tower_ui>();
        for (int i = 0; i < Containers.Length; i++)
        {
            Containers[i].Init(this); 
        }
    }

    public void SelectTower(Tower_ui tower)
    {
        tower.Play_Open_animation(world.GetCoins());
        if(tower != selected_tower)
            CloseTower(tower);
        else
            CloseTower(null);;
    }

    public void SelectNothing() => CloseTower(null);

    private void CloseTower(Tower_ui tower)
    {
        if( selected_tower != null)
            selected_tower.Play_Close_animation();
        selected_tower = tower;
        zoom_Controller.SetTarget( selected_tower?.tower_data.transform ) ;
    }
    
    public Tower_data_mono CreateTower(int id, Transform tr)
    {
        CloseTower(null);
        return creator.CreateTower(id,tr);
    }
    public Tower_data_mono UpgradeTower(int id, int lvl, Transform tr)
    {
        CloseTower(null);
        return creator.UpgradeTower(id, lvl, tr);
    }
    public Tower_data_mono DestroyTower( Transform tr, Tower_data_mono data)
    {
        CloseTower(null);
        return creator.DestroyTower(tr,data);
    }
}
