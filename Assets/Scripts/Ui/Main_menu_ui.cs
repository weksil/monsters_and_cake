﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main_menu_ui : MonoBehaviour
{
    public Level_db level_db;
    public GameObject lvl_list_item_prefab;
    public RectTransform List_content;
    public RectTransform List_viewport;
    public RectTransform Lvls_panel;

    private void Start() {
        level_db.Load();

        // first obj
        Instantiate(lvl_list_item_prefab, List_content)
                .GetComponent<Lvl_list_item>()
                .Init(this,0,level_db.lvl_scores[0], false);

        for (int i = 1; i < level_db.lvl_scores.Length; i++)
        {
            Instantiate(lvl_list_item_prefab, List_content)
                .GetComponent<Lvl_list_item>()
                .Init(this,i,level_db.lvl_scores[i], level_db.lvl_scores[i - 1 ] == Level_db.Score.nan);
        }
        StartCoroutine(FixLayout());
    }
    IEnumerator FixLayout()
    {
        yield return new WaitForEndOfFrame();
        var size = List_content.GetComponent<VerticalLayoutGroup>().preferredHeight;
        List_content.sizeDelta = new Vector2(List_content.sizeDelta.x, size + 100);
        List_content.anchoredPosition = new Vector2(0, -size/2 - 100);
        yield return new WaitForEndOfFrame();
        Lvls_panel.gameObject.SetActive(false);
        Lvls_panel.localPosition = new Vector3(Lvls_panel.localPosition.x, Lvls_panel.localPosition.y, 0 );

    }
    public void Load_level(int lvl_index)
    {
        level_db.PlayLevel(lvl_index);
    }
    public void Load_max_level()
    {
        level_db.Play();
    }

    public void ShowLevels() => Lvls_panel.gameObject.SetActive(!Lvls_panel.gameObject.activeSelf);
}
