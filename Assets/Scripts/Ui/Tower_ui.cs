﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tower_ui : MonoBehaviour
{

    public Animator animator_base;
    public Animator animator_upgrade;
    public Tower_data_mono tower_data;

    [Space]
    public Transform tr_camera;
    public float plane_distance = 2;

    [Space]
    public Color block_color;
    public Image upgrade_ico;
    public Collider upgrade_collider;

    [Space]
    public Tower_data_mono[] Create_towers;
    public Image[] Create_towers_ico;
    public Collider[] Create_towers_collider;
    public TextMeshProUGUI Upgrade_price;
    public Transform Range_circle;

    private bool is_closed = true;
    private int Close_trigger = Animator.StringToHash("Close");
    private Animator cur_animator;
    private Tower_global_ui global;


    public void Init(Tower_global_ui global_ui)
    {
        global = global_ui;
        global.world.Change_coins_e += CheckCoinColor;
        for (int i = 0; i < Create_towers.Length; i++)
        {
            Create_towers_ico[i].transform.parent.GetComponentInChildren<TextMeshProUGUI>().text = Create_towers[i].Price.ToString();

        }

        animator_base.gameObject.SetActive(false);
        animator_upgrade.gameObject.SetActive(false);

        // Math
        Transform canvas = animator_upgrade.transform;
        canvas.rotation = tr_camera.rotation;
        canvas.localPosition = Vector3.zero;
        canvas.position -= canvas.forward * plane_distance;

        canvas = animator_base.transform;
        canvas.rotation = tr_camera.rotation;
        canvas.localPosition = Vector3.zero;
        canvas.position -= canvas.forward * plane_distance;

    }
    public void Open_Aimation_Event()
    {
        if(cur_animator == null) return;
        is_closed = !is_closed;
        cur_animator.gameObject.SetActive(!is_closed);
        CheckCoinColor(global.world.GetCoins());
    }
    public void Play_Open_animation(int coins)
    {
        if(tower_data.Level != 0)
        {
            cur_animator = animator_upgrade;
            Upgrade_price.text = tower_data.Is_max_lvl ? "-" : tower_data.Ugrade_Price.ToString();
        }
        else
        {
            cur_animator =  animator_base;
        }
        cur_animator.gameObject.SetActive(true);
    }

    private void CheckCoinColor(int coins)
    {
        if(is_closed) return;
        if(tower_data.Level == 0)
        {
            for (int i = 0; i < Create_towers.Length; i++)
            {
                Create_towers_ico[i].color = coins >= Create_towers[i].Price ? Color.white : block_color;
                Create_towers_collider[i].enabled = coins >= Create_towers[i].Price;
            }
        }
        else
        {
            upgrade_ico.color = !tower_data.Is_max_lvl && (coins >= tower_data.Ugrade_Price) ? Color.white : block_color;
            upgrade_collider.enabled = !tower_data.Is_max_lvl && (coins >= tower_data.Ugrade_Price);
        }
    }
    // private void Math()
    // {
    //     Transform canvas = animator_upgrade.transform;
    //     canvas.rotation = tr_camera.rotation;
    //     canvas.localPosition = Vector3.zero;
    //     canvas.position -= canvas.forward * plane_distance;

    //     canvas = animator_base.transform;
    //     canvas.rotation = tr_camera.rotation;
    //     canvas.localPosition = Vector3.zero;
    //     canvas.position -= canvas.forward * plane_distance;
    // }
    public void Play_Close_animation()
    {
        cur_animator.SetTrigger(Close_trigger);
    }

    public void Open_gloabal()
    {
        global.SelectTower(this);
    }
    public void Close_gloabal()
    {
        global.SelectNothing();
    }
    public void CreateTower(int id)
    {
        tower_data.gameObject.SetActive(false);
        tower_data = global.CreateTower(id,transform);
        Range_circle.gameObject.SetActive(true);
        Range_circle.localScale = tower_data.Logic.GetRange();
    }
    public void UpgradeTower()
    {
        tower_data.gameObject.SetActive(false);
        tower_data = global.UpgradeTower(tower_data.Id, tower_data.Level,transform);
        Range_circle.localScale = tower_data.Logic.GetRange();
    }
    public void DestroyTower()
    {
        tower_data.gameObject.SetActive(false);
        tower_data = global.DestroyTower(transform, tower_data);    
        Range_circle.gameObject.SetActive(false);   
    }

    #if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.gray;
        for (int i = 5; i <= 12; i++)
        {
            Gizmos.DrawWireSphere(transform.position,i);
        }
        
    }
    #endif
}
