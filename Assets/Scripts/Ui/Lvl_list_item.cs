﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Lvl_list_item : MonoBehaviour
{
    public TextMeshProUGUI lvl;
    public GameObject[] stars;
    private Main_menu_ui main_menu;
    private int lvl_index;
    public void Init(Main_menu_ui main, int lvl, Level_db.Score score, bool block)
    {
        this.main_menu = main;
        this.lvl_index = lvl;
        this.lvl.text = (lvl+1).ToString();
        if(block)
        {
            this.GetComponent<Image>().raycastTarget = false;
            this.lvl.color = Color.gray;
        }
        for (int i = 0; i < (int)score; i++)
        {
            stars[i].SetActive(true);
        }
        
    }
    public void Click() => main_menu?.Load_level(lvl_index);
}
