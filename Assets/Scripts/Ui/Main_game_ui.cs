﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Main_game_ui : MonoBehaviour
{
    public Level_db level_Db;
    public World_info world;
    public Tower_global_ui tower_global_ui;
    public Time_controller time_Controller;
    public TextMeshProUGUI Speed_text;
    public GameObject Pause_go;
    public GameObject Play_go;
    public TextMeshProUGUI Coin_text;
    [Space]
    public GameObject Game_ui;
    public GameObject Stat_ui;
    public GameObject Score_ui;
    [Space]
    public GameObject[] Score_stars;
    public GameObject Play_next_btn;
    public Image Play_next_img;
    public GameObject Close_stat_btn;
    private bool can_play_next;

    private const int time_coin = 1;
    private bool game_over;
    private void Start() {
        world.Change_coins_e += ReprintCoin;
        ReprintCoin(world.GetCoins());
        game_over = false;
    }
    public void NextSpeed()
    {
        Speed_text.text = "x"+ time_Controller.NextGear();
    }
    public void Pause()
    {
        var state = time_Controller.Pause();
        Pause_go.SetActive(!state);
        Play_go.SetActive(state);
    }

    public void GameOver(float cake_hp, bool cake_alive)
    {
        time_Controller.Pause();
        tower_global_ui.SelectNothing();
        if(cake_alive)
        {
            var score = (Level_db.Score)(cake_hp*3);
            score = score == Level_db.Score.nan ? Level_db.Score.C : score;
            level_Db.SetScore(score);
            Score_stars[(int)score].SetActive(true);
            can_play_next = true;
        }
        else
        {
            Score_stars[0].SetActive(true);
            Play_next_img.color = Color.gray;
            can_play_next = false;
        }

        Close_stat_btn.SetActive(false);
        Play_next_btn.SetActive(true);
        game_over = true;
        Score_ui.SetActive(true);
        Open_stat_panel();

    }

    public void Open_stat_panel()
    {
        Game_ui.SetActive(false);
        Stat_ui.SetActive(true);
    }
    public void Close_stat_panel()
    {
        if(game_over) return;

        Game_ui.SetActive(true);
        Stat_ui.SetActive(false);
    }
    public void Reload() => level_Db.Reload();
    public void GoHome() => level_Db.Main_Menu();
    public void PlayNext()
    {
        if(!can_play_next) return;
        level_Db.PlayNext();
    }

    private void ReprintCoin(int coins) => Coin_text.text = coins.ToString();
}
