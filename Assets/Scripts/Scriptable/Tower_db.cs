﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tower_db", menuName = "TowerDefense/Tower_db")]
public class Tower_db : ScriptableObject {
    public Tower_levels[] Levels;
    public GameObject Get_next_lvl(Tower_data_mono tower) => Levels[tower.Id].levels[tower.Level + 1];
}   