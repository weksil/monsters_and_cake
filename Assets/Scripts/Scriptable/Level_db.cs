﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Level_db", menuName = "TowerDefense/Level_db")]
public class Level_db : ScriptableObject {
    private int loaded_lvl;
    public int loading_screen;
    public int main_menu;
    public Score[] lvl_scores;
    public int[] levels;
    private int cur_max_lvl;
    private int load_index;
    private void OnEnable() {
        Load();
    }
    public void Save()
    {
        using (FileStream fstream = new FileStream(Application.persistentDataPath + "/saves.td", FileMode.OpenOrCreate))
        {
            byte[] array = new byte[lvl_scores.Length];
            for (int i = 0; i < lvl_scores.Length; i++)
            {
                array[i] = (byte)lvl_scores[i];
            }
            fstream.Write(array, 0, array.Length);
        }

    }
    public void Load()
    {

        var path = Application.persistentDataPath + "/saves.td";
        if(!File.Exists(path)) 
        {
            lvl_scores = new Score[levels.Length];
            cur_max_lvl = 0;
            return;
        }

        using (FileStream fstream = new FileStream(path, FileMode.OpenOrCreate))
        {
            var size = new FileInfo(path).Length;
            byte[] array = new byte[size];
            fstream.Read(array, 0, array.Length);
            for (int i = 0; i < size; i++)
            {
                lvl_scores[i] = (Score)array[i];
            }

            cur_max_lvl = 0;
            for (int i = 1; i < size; i++)
            {
                if(lvl_scores[i] == Score.nan && lvl_scores[i-1] != Score.nan)
                {
                    cur_max_lvl = i;
                    break;
                }
            }
        }

    }
    public void Play()
    {
        PlayLevel(cur_max_lvl);
    }
    public void PlayNext()
    {
        PlayLevel((loaded_lvl + 1)%levels.Length);
    }
    public void PlayLevel(int id)
    {
        loaded_lvl = id;
        load_index = levels[loaded_lvl];
        SceneManager.LoadScene(loading_screen,LoadSceneMode.Single);
    }
    public void Load_lvl()
    {
        SceneManager.LoadSceneAsync(load_index,LoadSceneMode.Single);
    }
    public void SetScore(Score score)
    {
        lvl_scores[loaded_lvl] = lvl_scores[loaded_lvl]  > score ? lvl_scores[loaded_lvl] : score;
        #if !UNITY_EDITOR
        Save();
        #endif
    }
    public void Main_Menu() 
    {
        load_index = main_menu;
        SceneManager.LoadScene(loading_screen,LoadSceneMode.Single);
    }
    public void Reload() => PlayLevel(loaded_lvl);


    public enum Score
    {
        nan,
        C,
        B,
        A,
    }
}

